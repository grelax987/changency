module.exports = {
    development: {
        host: '',
        port: 8089,
        email: 'kosinov@rguard.ru'
    },
    production: {
        host: 'localhost',
        port: 8020,
        email: 'mail@changency.ru'
    }
};