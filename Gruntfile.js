module.exports = function (grunt) {
  grunt.initConfig({
      less: {
        default: {
          options: {
            compress: false,
            optimization: 0
          },
          files: {
            'public/src/style.css': 'public/src/less/style.less'
          }
        },
        production: {
          options: {
            optimization: 4,
            cleancss: true
          },
          files: {
            'public/src/style.css': 'public/src/less/style.less'
          }
        }
      },
      uncss: {
        dist: {
          options: {
            ignore: [
              '.top-nav-collapse',
              /\.navbar-custom-.*/,  //для стилей верхней плашки, например .navbar-custom-nlp
              /\.affix.*/,           //бутстраповский плавающий блок
              /\.post-tags.*/,       //вывод блока в посте - динамический
            ],
            stylesheets: ['/public/src/style.css'],
            htmlroot: './'
          },
          files: {
            'public/src/style.css': ['views/**/*.swig']
          }
        }
      },
      autoprefixer: {
        options: {
          browsers: ['last 2 versions', 'ie 8', 'ie 9']
        },
        your_target: {
          src: 'public/src/style.css'
        }
      },
      cssmin: {
        main: {
          options: {
            keepSpecialComments: 0
          },
          files: {
            'public/src/style.css': ['public/src/style.css']
          }
        }
      },
      filerev: {
        options: {
          encoding: 'utf8',
          algorithm: 'md5',
          length: 8
        },
        main: {
          src: [
            './public/src/img/**/*.{jpg,jpeg,gif,png}',
            './public/src/fonts/**/*.{eot,ttf,woff}',
            './public/src/style.css',
            './public/src/js/*.js',

            //VENDORS:
            './public/src/vendors/jquery/dist/jquery.min.js',
            './public/src/vendors/bootstrap/dist/js/bootstrap.min.js',
            './public/src/vendors/animate.css/animate.min.css',
            './public/src/vendors/wow/wow.min.js'
          ],
          dest: 'public/assets'
        },
        'blog-admin': {
          src: [
            'public/src_blog_admin/app.js',
            'public/src_blog_admin/style.css',

            //VENDORS:
            'public/src/vendors/bootstrap/dist/css/bootstrap.min.css',

            'public/src/vendors/pickadate/lib/compressed/picker.js',
            'public/src/vendors/pickadate/lib/compressed/picker.date.js',


            'public/src/vendors/pickadate/lib/compressed/themes/classic.css',
            'public/src/vendors/pickadate/lib/compressed/themes/classic.date.css'
          ],
          dest: 'public/assets/blog_admin'
        }
      },
      filerev_assets: {
        dist: {
          options: {
            dest: 'tmp/assets.json',
            cwd: 'public/',
            prefix: ''
          }
        }
      },
      replace: {
        all: {
          src: [
            './public/assets/style.*.css',
            './views/**/*.swig',
            './server.js'
          ],
          overwrite: true,
          replacements: 'tmp/assets.json',
          replacementsIsFile: true
        }
      },
      clean: ['tmp/assets.json'],
      watch: {
        styles: {
          files: ['public/src/less/**/*.less'],
          tasks: ['less:default'],
          options: {
            nospawn: true
          }
        }
      }
    }
  );

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.loadNpmTasks('grunt-filerev');
  grunt.loadNpmTasks('grunt-filerev-assets');
  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-text-replace');

  grunt.registerTask('default', ['less:default', 'watch']);
  grunt.registerTask('build', ['less:production', /*'uncss',*/ 'autoprefixer', 'cssmin', 'filerev', 'filerev_assets', 'replace', 'clean']);
};