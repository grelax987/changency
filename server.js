var express = require('express');
var serveStatic = require('serve-static');
var bodyParser = require('body-parser');
var swig = require('swig');
var env = process.env.NODE_ENV || 'development';
var app = express();

var events = require('events');
var appEmitter = new events.EventEmitter();

app.engine('swig', swig.renderFile);
app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + '/views');
app.set('view engine', 'swig');

var helpers = require('./lib/helpers');
for (var helper in helpers) {
    if (!helpers.hasOwnProperty(helper)) {
        continue;
    }
    swig.setFilter(helper, helpers[helper]);
}

if (env === 'development') {
    app.set('view cache', false);
    swig.setDefaults({cache: false});
} else if (env === 'production') {
    app.set('view cache', true);
    swig.setDefaults({cache: 'memory'});
}


var config = require('./config/config.js');

app.use(serveStatic('public/'));
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded());
app.use(function (req, res, next) {
    res.locals.og = {
        title: 'Мне нравится Changency',
        image: 'http://changency.ru/src/img/social/og.png',
        url: req.url,
        description: null
    };

    res.locals.meta = {
        title: 'Коучинговое агентство Евгения Ульянова - Changency',
        description: 'Коучинговое агентство Евгения Ульянова - Changency'
    };
    next();
});

app.use(function (req, res, next) {
    res.locals.env = env;
    next();
});

//блог
app.use("/blog", require("./blog/server"));

//статический раутинг
app.use("/", require("./routes/static"));

//почтовый раутер
app.use("/mail", require("./routes/mail"));

app.use("/event/signup", require("./routes/sign-up")(appEmitter));

app.use(function (req, res, next) {

    res.status(404);

    if (req.accepts('html')) {
        res.render('404', {url: req.url});
        return;
    }

    if (req.accepts('json')) {
        res.send({error: 'Not found'});
        return;
    }

    res.type('txt').send('Страница не найдена');
});

app.use(function (err, req, res, next) {
    console.log(err);
    res.status(err.status || 500).send('Server error');
});

app.listen(config[env].port, config[env].host, function () {
    console.log("App started on port", config[env].port);
});



require('./lib/event-notify.js')(appEmitter);
require('./lib/event-sign-up.js')(appEmitter);