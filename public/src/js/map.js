(function (window) {

  window.mapInitialize = function () {
    var options = {
      zoom: 16,
      panControl: false,
      scaleControl: true,
      overviewMapControl: false,
      mapTypeControl: false,
      zoomControl: true,
      navigationControl: false,
      streetViewControl: false,
      disableDoubleClickZoom: false,
      scrollwheel: false,
      center: new google.maps.LatLng(59.940641, 30.356490)
    };


    var map = new google.maps.Map(document.getElementById('gmap_canvas'), options);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(59.940641, 30.356490),
      map: map,
      draggable: false,
      //icon: "/static/i/map.png"
    });
  };

  function loadScript() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&callback=mapInitialize';
    document.body.appendChild(script);
  }

  window.onload = loadScript;

})(window);
