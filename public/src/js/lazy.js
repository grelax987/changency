(function(window) {
  window.scroller = {
    isLoading: false,
    container: null,
    loader: null,
    init: function(container, loader) {
      this.container = container;
      this.loader = loader;
    },
    isStop: false,
    stop: function() {
      this.isStop = true;
    },
    getData: function(page, callback) {
      var that = this;
      if (that.isLoading || that.isStop) {
        return;
      }
      that.loader.fadeIn();
      that.isLoading = true;

      $.ajax({
        url: document.location.href + "/" + page,
        error: function(xhr, text, e) {
          that.loader.fadeOut();
          if (!that.isStop) {
            callback();
            that.stop();
            that.loader.fadeOut();
            that.isLoading = false;
          }
        },
        success: function(data) {
          that.loader.fadeOut();
          that.isLoading = false;
          that.container.append(data);
          if (data.length <= 0) {
            that.stop();
          }
          callback();
        }
      });
    }
  };
})(window);

$(document).ready(function() {
  if (!$('.lazy-block').length || !$('.loader').length) {
    return;
  }

  var screenHeight = $(window).height();
  var container = $('.lazy-block');
  window.scroller.init(container, $('.loader'));
  var page = 1;

  $(window).scroll(function() {
    var scroll = $(this).scrollTop();
    var divHeight = container.height();
    var totalHeight = screenHeight + scroll;
    var diff = divHeight - totalHeight;
    if (diff < 10) {
      window.scroller.getData(page + 1, function(err, res) {
        if (err) return console.log(err);
        page++;
      });
    }
  });
});