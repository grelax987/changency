$(function ($) {

  var spaceForNavbar = $('.space-for-navabar').length != 0;

  var top = $('.top');

  function makeTopBlockOnAllHeight() {
    var x = $(window).height();
    if (x < 400) x = 400;
    if (spaceForNavbar) x -= 50;
    top.height(x);
  }

  makeTopBlockOnAllHeight();

  $(window).resize(function() {
    makeTopBlockOnAllHeight();
  });

  var navbarFixedTop = $(".navbar-fixed-top");

  if (navbarFixedTop.hasClass('top-nav-collapse')) {
    return;
  }

  var navbar = $(".navbar");
  var recalculateNavbarPosition = function() {
    if (navbar.offset().top > 50) {
      navbarFixedTop.addClass("top-nav-collapse");
    } else {
      navbarFixedTop.removeClass("top-nav-collapse");
    }
  };

  setTimeout(recalculateNavbarPosition, 20);

  setTimeout(function () {
    navbar.addClass('animation');
    $(window).scroll(function(){
      recalculateNavbarPosition();
    });
  }, 500);



});
