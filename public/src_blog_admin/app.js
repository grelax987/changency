(function (document) {

  var coverUploadInput = document.getElementById('cover-upload-input');
  var coverPreviewDiv = document.getElementById("cover-preview");
  var coverPreviewImg = coverPreviewDiv.getElementsByTagName("img")[0];
  var coverDelete = document.getElementsByClassName("cover-delete")[0];

  var cover = document.getElementById('cover');

  coverDelete.addEventListener('click', function (event) {
    event.preventDefault();
    cover.value = '';
    deleteFile(this.getAttribute('url'), function () {
      coverUploadInput.classList.remove('hidden');
      coverDelete.classList.add('hidden');
      coverPreviewImg.src = "";
    });
  }, false);

  coverUploadInput.addEventListener('change', function (event) {
    var file = this.files[0];

    coverPreviewDiv.classList.add('loader');
    uploadFile(file, function () {
      var result = JSON.parse(this.responseText);

      cover.value = result.path;
      coverDelete.setAttribute('url', result.path);

      coverUploadInput.classList.add('hidden');
      coverPreviewDiv.classList.remove('loader');
      coverPreviewImg.src = "/upload/" + result.path;
      coverDelete.classList.remove('hidden');
      coverPreviewDiv.classList.add('cover-delete');
    });

  }, false);

  function uploadFile(file, onLoadCallback) {
    var fd = new FormData();
    fd.append(file.name, file);
    var xhr = new XMLHttpRequest();

    xhr.onload = onLoadCallback;

    xhr.addEventListener("load", function (event) {
      console.log("LOAD", arguments);

    }, false);
    xhr.addEventListener("error", function () {
      console.log("ERROR");
    }, false);
    xhr.open("POST", "/blog/yamb/image");
    xhr.send(fd);
  }


  function deleteFile(url, onLoadCallback) {

    var xhr = new XMLHttpRequest();
    xhr.onload = onLoadCallback;

    xhr.addEventListener("load", function (event) {
      console.log("LOAD", arguments);
    }, false);

    xhr.addEventListener("error", function () {
      console.log("ERROR");
    }, false);

    xhr.open("DELETE", "/blog/yamb/image/" + url);
    xhr.send();
  }

})(document);