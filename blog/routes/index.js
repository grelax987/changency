"use strict";

var middleware = require('./../middleware');

module.exports = function(app) {
  app.param('yamb', middleware.yamb);


  //middleware.redirects(app);

  require('./admin')(app, middleware);
  require('./yamb')(app, middleware);
};