"use strict";

var auth = require('http-auth');
var basic = auth.basic(
  { realm: "Password?"},
  function (username, password, callback) {
    callback(username === "changency" && password === "changency557");
  }
);

module.exports = function(app, middleware) {
  var config = app.get('config');
  var action = require('./../controllers/admin');

  action.uploadDir = config.get('uploadDirName');

  action.galleryDirName = config.get('galleryDirName');
  action.galleryPublicDirName = config.get('galleryPublicDirName');

  app.all('/yamb/*', auth.connect(basic), function(request, response, next) {
    next();
  });

  app.post('/yamb/image', action.saveImage);
  app.delete('/yamb/image/:name', action.removeImage);

  var gallery = action.gallery;
  app.get('/yamb/gallery', gallery.list);
  app.post('/yamb/gallery/upload', gallery.upload);

  app.get('/yamb', action.index);

  app.get('/yamb/create', action.create);
  app.post('/yamb/create', action.save);

  app.get('/yamb/:yamb', action.show);
  app.post('/yamb/:yamb', action.update);

  app.get('/yamb/:yamb/remove', action.remove);
};