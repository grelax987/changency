"use strict";
var authors = require('../config/authors');
var multiparty = require('multiparty');
var path = require("path");
var fs = require('fs');
var gm = require('gm');
var htmlRegex = /(<([^>]+)>)/ig;

var proccesBody = function(req) {
  var body = req.body;

  var author = body.author || Changency;

  body.author = authors.filter(function(item) {
    return (item.name === author);
  }).shift();

  if (!body.meta.title || body.social.title.length === 0) {
    body.meta.title = body.title + ' – Блог – Changency';
  }

  if (!body.social.title || body.social.title.length === 0) {
    body.social.title = body.title;
  }

  var desc = body.preview.replace(htmlRegex, '');

  if (!body.social.description || body.social.description.length === 0) {
    body.social.description = desc;
  }

  if (!body.meta.description || body.meta.description.length === 0) {
    body.meta.description = desc;
  }

  return body;
};

var processOgImage = function(originalPath) {
  return function(cb) {
    let fileExt = path.extname(originalPath);
    let fileBaseName = path.basename(originalPath, fileExt);
    let newName = fileBaseName + "_og" + fileExt;
    let imageCroppedPath = path.join(path.dirname(originalPath), newName);

    gm(originalPath).noProfile().thumb(200, 200, imageCroppedPath, 90, function (err) {
      cb(err, 'http://changency.ru/upload/' + newName);
    });
  };
};

exports.index = function *(req, res) {
  var posts = yield res.yamb.fetchAll();

  res.render('blog/admin/index', {
    posts: posts
  });
};

exports.create = function *(req, res) {
  var related = yield res.yamb.fetchAll(null, {sort: 'name'});

  res.render('blog/admin/show', {
    post: {},
    related: related
  });
};

exports.saveImage = function *(req, res) {
  let opts = {
    uploadDir: exports.uploadDir
  };
  let form = new multiparty.Form(opts);

  form.parse(req);
  form.on('file', function (name, file) {
    let fileExt = path.extname(file.path);
    let fileBaseName = path.basename(file.path, fileExt);
    let imageCroppedPath = path.join(path.dirname(file.path), fileBaseName + "_crop" + fileExt);

    gm(file.path).size(function(err, value){
      console.log(err);
      let width = value.width;
      let height = value.height;

      if (width > 976 && height > 696) {
        width = 976;
        height = 696
      } else {
        width = 488;
        height = 348;
      }

      gm(file.path)
        .noProfile()
        .thumb(width, height, imageCroppedPath, 90, function (err) {
          if (err) {
            console.warn('Resize err', file.path, "=>", imageCroppedPath, err);
            return res.json({ err: false, path: path.basename(file.path)});
          }

          try {
            fs.unlinkSync(file.path);
          } catch (err) {
            console.warn("Failed to delete:", file.path, err);
          }

          res.json({ err: false, path: path.basename(imageCroppedPath)});
        });

    });


  });

  form.on('error', function (err) {
    console.error('File upload failed', err);
    res.json({ err: true});
  });
};


exports.removeImage = function *(req, res) {
  var file = path.join(exports.uploadDir, req.params.name);
  try {
    fs.unlinkSync(file);
  } catch (err) {
    console.warn("Failed to delete", file, err);
  }
  res.json(req.params.name);
};

exports.save = function *(req, res) {
  var body = proccesBody(req);

  if (body.cover && body.cover.length > 0) {
    var path = exports.uploadDir + '/' + body.cover;
    var ogImg =  yield processOgImage(path);
    body.social.image = ogImg;
  }

  var post = res.yamb.create(body);

  try {
    yield post.save();
  } catch (err) {
    return res.redirect('/blog/yamb/create?error=' + err.message);
  }

  res.redirect('/blog/yamb/' + post.id);
};

exports.show = function *(req, res) {
  var related = yield res.yamb.fetchAll({id: {$ne: res.post.id}}, {sort: 'name'});

  res.render('blog/admin/show', {
    related: related
  });
};

exports.update = function *(req, res) {
  var body = proccesBody(req);
  if (!body.related) {
    body.related = [];
  }
  if (!body.active) {
    body.active = false;
  }

  if (body.cover && body.cover.length > 0) {
    var path = exports.uploadDir + '/' + body.cover;
    var ogImg = yield processOgImage(path);
    body.social.image = ogImg;
  }

  var post = res.post.update(body);
  try {
    yield post.save();
  } catch(e) {
    return res.redirect('/blog/yamb/' + post.id + '?error=' + err.message);
  }

  return res.redirect('/blog/yamb/' + post.id);
};

exports.remove = function *(req, res) {
  var result = yield res.post.remove();
  delete res.post;

  res.redirect('/blog/yamb?result=' + result);
};

exports.gallery = {
  list: function *(request, response, next) {
    fs.readdir(exports.galleryDirName, function(err, res) {
      if (err) return next(err);

      response.render('blog/admin/gallery/list', {
        dirname: exports.galleryPublicDirName,
        imgs: res.filter(function(item) {
          return !(item.substr(0, 1) == '.');
        })
      });
    });
  },
  upload: function *(request, response, next) {
    let form = new multiparty.Form({
      uploadDir: exports.galleryDirName
    });

    form.parse(request);
    form.on('file', function (name, file) {
      return response.redirect('/blog/yamb/gallery');
    });

    form.on('error', function (err) {
      console.log(err);
      return next(err);
    });
  }
};
