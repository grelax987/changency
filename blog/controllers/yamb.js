"use strict";

exports.index = function *(req, res) {
  var page = req.params.page || 1;
  var tag = req.params.tag || '';
  var limit = 20;

  if (tag.length > 0 && parseInt(tag) > 0) {
    page = parseInt(tag, 10);
    tag = '';
  }

  if (page <= 0) {
    page = 1;
  }

  var params = {
    active: true,
  };

  var options = {
    limit: limit,
    skip: (page - 1) * limit
  };

  if (tag.length > 0) {
    if (tag === 'post' || tag === 'tagged') {
      return res.redirect('/blog');
    }
    params.tags = {
      $in: [req.params.tag]
    }
  }

  var posts = yield res.yamb.fetchAll(params, options);
  var view = (!req.xhr) ? 'blog/index' : 'blog/lazy';

  res.render(view, {
    posts: posts,
    page: (page == 1) ? '' : page,
    tag_active: tag
  });
};

exports.show = function *(req, res) {
  var uri = [
    req.params.year,
    req.params.month,
    req.params.slug
  ];

  var post = yield res.yamb.fetch({uri: '/' + uri.join('/')});
  var related = [];
  if (post.similar !== undefined) {
    related = yield post.similar();
  }

  if (post.social) {
    res.locals.og = {
      title:  post.social.title || 'Changency',
      image:  post.social.image || 'http://changency.ru/assets/og.png',
      url: req.url,
      description: post.social.description || 'Changency - Профессионалы изменений'
    };
  }

  if (post.meta) {
    res.locals.meta = {
      title: post.meta.title || 'Changency',
      description: post.meta.description || 'Changency - Профессионалы изменений'
    };
  }

  res.render('blog/post', {
    post: post,
    related: related
  });
};