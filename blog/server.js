"use strict";
var swig    = require('swig');
var config  = require('./config');
var authors  = require('./config/authors');
var tags  = require('./config/tags');
var viewsDir = require("path").resolve(__dirname, '../views');

var express = require('express');
var thunkify = require('co-express');

var app = module.exports = thunkify(express());
app.set("config", config);
app.set('views', viewsDir);
var mongo = require('co-easymongo')({
  dbname: config.get('dbname')
});

var routes = require('./routes');

app.use(function(req, res, next) {
  res.locals.authors = authors;
  res.locals.tags = tags;
  res.locals.host = config.get('host');
  next();
});

app.response.yamb = require('./yamb/index.js')({
  storage: mongo.collection(config.get('collection')),
  yapi: config.get('yapi')
});

app.response.error = require('./lib/notfound');

routes(app);

return app;
