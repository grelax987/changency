"use strict";

var fs = require('fs');
var path = require('path');

var nconf = module.exports = require('nconf');
var env = process.env.NODE_ENV || 'development';
var config = __dirname + '/app.' + env + '.json';

nconf.overrides({'env': env});
nconf.env(['port', 'yapi']);

if (fs.existsSync(config)) {
  nconf.file('custom', config);
}

nconf.file(__dirname + '/app.json');

var uploadDirName = path.normalize(path.dirname(path.dirname(__dirname)) + '/public/upload');
nconf.set('uploadDirName', uploadDirName);

nconf.set('galleryDirName', uploadDirName + '/gallery');
nconf.set('galleryPublicDirName', '/upload/gallery');