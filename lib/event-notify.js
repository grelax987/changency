var config = require('../config/config.js');
var env = process.env.NODE_ENV || 'development';

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'Yandex',
    auth: {
        user: 'bot@changency.ru',
        pass: 'asjdM12pqw,1-2'
    }
});

module.exports = function(appEmitter) {

    var eventNotifyHandler = function(msg) {

        var mailOptions = {
            from: 'Bot Changency  <bot@changency.ru>',
            to: config[env].email,
            subject: 'Новая заявка: ' + msg['type'],
            text: JSON.stringify(msg)
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                appEmitter.emit('email-send-error', msg, error);
            } else {
                appEmitter.emit('email-send-successful', msg, info);
            }
        });

    };

    appEmitter.on('event-sign-up-training', eventNotifyHandler);
    appEmitter.on('event-sign-up-presentation', eventNotifyHandler);
};