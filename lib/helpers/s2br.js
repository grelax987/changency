"use strict";

module.exports = function s2br(str) {
  return str.split(' ').join('<br>');
};