"use strict";

var util = require('util'),
template = '<option value="%s"%s>%s</option>';

module.exports = function formoption(arr, post) {
  var html = '', tags = false, select;

  if (post && post.tags && post.tags.length > 0) {
    tags = post.tags;
  }

  for (var i = 0, length = arr.length; i<length; i++) {
    if (tags && tags.indexOf('' + arr[i].id) !== -1) {
      select = ' selected';
    } else {
      select = '';
    }

    html += util.format(template, arr[i].id, select, arr[i].title);
  }

  return html;
};