"use strict";

var util = require('util'),

template = '<option value="%s"%s>%s</option>';

module.exports = function formauthor(arr, post) {
  var html = '', author = false, select;

  if (post && post.author && post.author.name) {
    author = post.author.name;
  }

  for (var i = 0, length = arr.length; i < length; i++) {
    if (author === arr[i].name) {
      select = ' selected';
    } else {
      select = '';
    }

    html += util.format(template, arr[i].name, select, arr[i].name);
  }

  return html;
};