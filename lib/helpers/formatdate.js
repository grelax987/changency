"use strict";

var moment = require('moment');
moment.lang('ru');
var utils = require('./utils');

module.exports = function formatDate(date, options) {
  options = utils.opts(options);

  if (!date || arguments.length === 1) {
    date = new Date();
  } else {
    date = new Date(date);

    if (!utils.is.date(date)) {
      throw new Error('Invalid date');
    }
  }

  if (!utils.is.exst(options.htmltag)) {
    options.htmltag = true;
  }

  var text = moment(date).format("D MMMM YYYY");


  if (options.htmltag) {
    text = '<time datetime="' + date.toISOString() + '">' + text + '</time>';
  }

  if (options.admin) {
    return moment(date).format("DD.MM.YYYY");
  }

  return text;
};