"use strict";

var util = require('util'),
template = '<li><a href="/blog/%s">%s</a></li>';

module.exports = function posttags(arr, allTags) {
  var html = '';

  for (var i = 0; i < allTags.length; i++) {
    var tag = allTags[i];
    if (arr.indexOf(tag.id) != -1) {
      html += util.format(template, tag.id, tag.title);
    }
  }

  return html;
};