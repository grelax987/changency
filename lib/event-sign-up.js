var Store = require("jfs");
var path = require('path');
var storagePath = path.join(__dirname, "..", 'data');
var jdb = new Store(storagePath);

var eventSignUpHandler = function (msg) {

    var data = [msg];

    jdb.get("nlp-" + msg['type'], function (err, oldData) {
        if (err) console.log(err);

        if (oldData) {
            data = data.concat(oldData)
        }

        jdb.save("nlp-" + msg['type'], data, function (err) {
            if (err) console.log(err);
        });
    });

};

module.exports = function(appEmitter) {
    appEmitter.on('event-sign-up-training', eventSignUpHandler);
    appEmitter.on('event-sign-up-presentation', eventSignUpHandler);
};