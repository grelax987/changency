var validator = require('validator');
var express = require('express');
var app = express();

var someFieldsNotString = function (fields, req) {
    return fields.some(function (key) {
        return typeof req.body[key] !== 'string';
    })
};

var fields = ['name', 'email', 'phone', 'type'];

module.exports = function(appEmitter) {

    app.post('/', function(req, response, next) {

        if (someFieldsNotString(fields, req)) {
            return response.status(400).send();
        }

        if (req.body.type !== 'presentation' && req.body.type !== 'training') {
            return response.status(400).send();
        }

        var msg = {};
        var err = {};
        msg.name = req.body.name;
        msg.email = req.body.email;
        msg.phone = req.body.phone;
        msg.type= req.body.type;

        if (!validator.isEmail(msg.email)) {
            err.email = "Неверный email"
        }
        if (msg.name.length < 4) {
            err.name = "Введите ваше имя"
        }
        if (msg.phone.length < 4) {
            err.phone = "Телефон не ваш"
        }
        if (Object.keys(err).length) {
            console.log('err', new Date(), msg);
            return response.status(400).json({err: err});
        }
        response.status(200).json({result: true});

        appEmitter.emit('event-sign-up-' + msg.type, msg);

    });

    return app;

};