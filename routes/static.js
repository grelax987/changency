var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var express = require('express');
var staticRouter = express();
var staticViewsDir = path.resolve(__dirname, '../views/static');

var validFileExtensions = ['.swig','.jade','.html', '.xml'];

function isValidFileExt(fileName) {
  var fileExt = path.extname(fileName);
  return validFileExtensions.some(function(validExt) {
    return fileExt === validExt;
  });
}

function baseFileName(fileName) {
  return fileName.substr(0, fileName.lastIndexOf('.'));
}

function isIndexFile(fileName) {
  return fileName.indexOf('index') === 0 && validFileExtensions.some(function(extension) {
    return fileName.indexOf(extension) === 'index'.length &&
           fileName.length === ('index' + extension).length;
  });
}

function cutLastSlash(batePath) {
  return batePath.substr(0, batePath.lastIndexOf('/'));
}

var routes = function (viewsDir, batePath) {

  var files =  fs.readdirSync(viewsDir);
  var result = {};

  for (var i = 0; i < files.length; i++) {
    var fileName = files[i];
    var filePath = path.resolve(viewsDir + '/' + fileName);
    var stat = fs.statSync(filePath);

    if (stat.isDirectory())
      _.extend(result, routes(filePath, batePath + fileName + '/'));

    if (!stat.isFile() || !isValidFileExt(fileName))
      continue;

    if (isIndexFile(fileName) && batePath === '/')
      result[batePath] = filePath;
    else if (isIndexFile(fileName))
      result[cutLastSlash(batePath)] = filePath;
    else
      result[batePath + baseFileName(fileName)] = filePath;
  }

  return result;
};


var staticRoutes = routes(staticViewsDir, '/');

for (var route in staticRoutes) {
  if (!staticRoutes.hasOwnProperty(route)) {
    continue;
  }
  (function (route) {
    staticRouter.get(route, function (request, response) {
      response.render(staticRoutes[route]);
    })
  })(route);

}


module.exports = staticRouter;